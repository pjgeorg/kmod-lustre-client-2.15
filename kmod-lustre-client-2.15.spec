%global pkg lustre-client-2.15

%global kernel 4.18.0-348.el8
%global baserelease 1
%global upstream 2.15.1

%global _use_internal_dependency_generator 0
%global __find_requires /usr/lib/rpm/redhat/find-requires
%global __find_provides /usr/lib/rpm/redhat/find-provides

%global debug_package %{nil}

%global __spec_install_post \
  %{?__debug_package:%{__debug_install_post}} \
  %{__arch_install_post} \
  %{__os_install_post} \
  %{__mod_compress_install_post}

%global __mod_compress_install_post find %{buildroot}/lib/modules -type f -name \*.ko -exec xz \{\} \\;


Name:             kmod-%{pkg}
Epoch:            1
Version:          %(echo %{kernel} | sed 's/-/~/g; s/\.el.*$//g')
Release:          %{upstream}~%{baserelease}%{?dist}
Summary:          Lustre File System

License:          GPLv2
URL:              https://wiki.whamcloud.com/

Source0:          lustre-%{upstream}.tar.gz

ExclusiveArch:    x86_64 aarch64 ppc64le

BuildRequires:    elfutils-libelf-devel
BuildRequires:    gcc
BuildRequires:    kernel-rpm-macros
BuildRequires:    kmod
BuildRequires:    make
BuildRequires:    redhat-rpm-config
BuildRequires:    xz

BuildRequires:    kernel-abi-stablelists = %{kernel}
BuildRequires:    kernel-devel = %{kernel}
BuildRequires:    kernel-devel-uname-r = %{kernel}.%{_arch}

Requires:         kernel >= %{kernel}
Requires:         kernel-uname-r >= %{kernel}.%{_arch}
Requires:         kernel-modules >= %{kernel}
Requires:         kernel-modules-uname-r >= %{kernel}.%{_arch}

Provides:         installonlypkg(kernel-module)
Provides:         kernel-modules >= %{kernel}.%{_arch}

Requires(posttrans): %{_sbindir}/depmod
Requires(postun):    %{_sbindir}/depmod

Requires(posttrans): %{_sbindir}/weak-modules
Requires(postun):    %{_sbindir}/weak-modules

Requires(posttrans): %{_bindir}/sort
Requires(postun):    %{_bindir}/sort


BuildRequires:    bison
BuildRequires:    flex
BuildRequires:    krb5-devel
BuildRequires:    libmount-devel
BuildRequires:    libnl3-devel
BuildRequires:    libselinux-devel
BuildRequires:    libtool
BuildRequires:    libyaml-devel
BuildRequires:    openssl-devel
BuildRequires:    pkgconfig
BuildRequires:    python3-devel >= 3.6.0
BuildRequires:    swig
BuildRequires:    zlib-devel

Provides:         kmod-lustre-client = %{?epoch:%{epoch}:}%{version}-%{release}
Conflicts:        kmod-lustre-client-2.12

Supplements:      lustre-client
Recommends:       lustre-client

%if %{epoch}
Obsoletes:        kmod-%{pkg} < %{epoch}:
%endif
Obsoletes:        kmod-%{pkg} = %{?epoch:%{epoch}:}%{version}


%description
Lustre File System kernel modules.


%prep
%autosetup -p1 -n lustre-%{upstream}


%build
%define optflags -g -O2 -Werror
%undefine _annotated_build
%undefine _hardened_build

%configure \
    --disable-doc \
    --disable-iokit \
    --disable-ldiskfs \
    --disable-manpages \
    --disable-server \
    --disable-snmp \
    --disable-tests \
    --disable-utils \
    --enable-gss \
    --enable-gss-keyring \
    --enable-modules \
    --with-systemdsystemunitdir=%{_unitdir} \
    --with-linux=/usr/src/kernels/%{kernel}.%{_arch} \
    --with-linux-obj=/usr/src/kernels/%{kernel}.%{_arch} \
    --with-kmp-moddir=extra

%{__make} -C /usr/src/kernels/%{kernel}.%{_arch} %{?_smp_mflags} M=$PWD modules


%install
%{__install} -D -t %{buildroot}/lib/modules/%{kernel}.%{_arch}/extra/lustre-client/fs lustre/fid/fid.ko
%{__install} -D -t %{buildroot}/lib/modules/%{kernel}.%{_arch}/extra/lustre-client/fs lustre/fld/fld.ko
%{__install} -D -t %{buildroot}/lib/modules/%{kernel}.%{_arch}/extra/lustre-client/fs lustre/llite/lustre.ko
%{__install} -D -t %{buildroot}/lib/modules/%{kernel}.%{_arch}/extra/lustre-client/fs lustre/lmv/lmv.ko
%{__install} -D -t %{buildroot}/lib/modules/%{kernel}.%{_arch}/extra/lustre-client/fs lustre/lov/lov.ko
%{__install} -D -t %{buildroot}/lib/modules/%{kernel}.%{_arch}/extra/lustre-client/fs lustre/mdc/mdc.ko
%{__install} -D -t %{buildroot}/lib/modules/%{kernel}.%{_arch}/extra/lustre-client/fs lustre/mgc/mgc.ko
%{__install} -D -t %{buildroot}/lib/modules/%{kernel}.%{_arch}/extra/lustre-client/fs lustre/obdclass/obdclass.ko
%{__install} -D -t %{buildroot}/lib/modules/%{kernel}.%{_arch}/extra/lustre-client/fs lustre/obdecho/obdecho.ko
%{__install} -D -t %{buildroot}/lib/modules/%{kernel}.%{_arch}/extra/lustre-client/fs lustre/osc/osc.ko
%{__install} -D -t %{buildroot}/lib/modules/%{kernel}.%{_arch}/extra/lustre-client/fs lustre/ptlrpc/gss/ptlrpc_gss.ko
%{__install} -D -t %{buildroot}/lib/modules/%{kernel}.%{_arch}/extra/lustre-client/fs lustre/ptlrpc/ptlrpc.ko
%{__install} -D -t %{buildroot}/lib/modules/%{kernel}.%{_arch}/extra/lustre-client/net libcfs/libcfs/libcfs.ko
%{__install} -D -t %{buildroot}/lib/modules/%{kernel}.%{_arch}/extra/lustre-client/net lnet/klnds/o2iblnd/ko2iblnd.ko
%{__install} -D -t %{buildroot}/lib/modules/%{kernel}.%{_arch}/extra/lustre-client/net lnet/klnds/socklnd/ksocklnd.ko
%{__install} -D -t %{buildroot}/lib/modules/%{kernel}.%{_arch}/extra/lustre-client/net lnet/lnet/lnet.ko

# Make .ko objects temporarily executable for automatic stripping
find %{buildroot}/lib/modules -type f -name \*.ko -exec chmod u+x \{\} \+


%clean
%{__rm} -rf %{buildroot}


%triggerin -- %{name}
mkdir -p %{_localstatedir}/lib/rpm-state/sig-kmods
printf '%s\n' "/lib/modules/%{kernel}.%{_arch}/extra/lustre-client/fs/fid.ko.xz" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
printf '%s\n' "/lib/modules/%{kernel}.%{_arch}/extra/lustre-client/fs/fld.ko.xz" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
printf '%s\n' "/lib/modules/%{kernel}.%{_arch}/extra/lustre-client/fs/lustre.ko.xz" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
printf '%s\n' "/lib/modules/%{kernel}.%{_arch}/extra/lustre-client/fs/lmv.ko.xz" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
printf '%s\n' "/lib/modules/%{kernel}.%{_arch}/extra/lustre-client/fs/lov.ko.xz" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
printf '%s\n' "/lib/modules/%{kernel}.%{_arch}/extra/lustre-client/fs/mdc.ko.xz" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
printf '%s\n' "/lib/modules/%{kernel}.%{_arch}/extra/lustre-client/fs/mgc.ko.xz" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
printf '%s\n' "/lib/modules/%{kernel}.%{_arch}/extra/lustre-client/fs/obdclass.ko.xz" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
printf '%s\n' "/lib/modules/%{kernel}.%{_arch}/extra/lustre-client/fs/obdecho.ko.xz" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
printf '%s\n' "/lib/modules/%{kernel}.%{_arch}/extra/lustre-client/fs/osc.ko.xz" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
printf '%s\n' "/lib/modules/%{kernel}.%{_arch}/extra/lustre-client/fs/ptlrpc_gss.ko.xz" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
printf '%s\n' "/lib/modules/%{kernel}.%{_arch}/extra/lustre-client/fs/pc/ptlrpc.ko.xz" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
printf '%s\n' "/lib/modules/%{kernel}.%{_arch}/extra/lustre-client/net/libcfs.ko.xz" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
printf '%s\n' "/lib/modules/%{kernel}.%{_arch}/extra/lustre-client/net/ko2iblnd.ko.xz" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
printf '%s\n' "/lib/modules/%{kernel}.%{_arch}/extra/lustre-client/net/ksocklnd.ko.xz" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
printf '%s\n' "/lib/modules/%{kernel}.%{_arch}/extra/lustre-client/net/lnet.ko.xz" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules


%triggerun -- %{name}
mkdir -p %{_localstatedir}/lib/rpm-state/sig-kmods
printf '%s\n' "/lib/modules/%{kernel}.%{_arch}/extra/lustre-client/fs/fid.ko.xz" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
printf '%s\n' "/lib/modules/%{kernel}.%{_arch}/extra/lustre-client/fs/fld.ko.xz" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
printf '%s\n' "/lib/modules/%{kernel}.%{_arch}/extra/lustre-client/fs/lustre.ko.xz" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
printf '%s\n' "/lib/modules/%{kernel}.%{_arch}/extra/lustre-client/fs/lmv.ko.xz" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
printf '%s\n' "/lib/modules/%{kernel}.%{_arch}/extra/lustre-client/fs/lov.ko.xz" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
printf '%s\n' "/lib/modules/%{kernel}.%{_arch}/extra/lustre-client/fs/mdc.ko.xz" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
printf '%s\n' "/lib/modules/%{kernel}.%{_arch}/extra/lustre-client/fs/mgc.ko.xz" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
printf '%s\n' "/lib/modules/%{kernel}.%{_arch}/extra/lustre-client/fs/obdclass.ko.xz" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
printf '%s\n' "/lib/modules/%{kernel}.%{_arch}/extra/lustre-client/fs/obdecho.ko.xz" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
printf '%s\n' "/lib/modules/%{kernel}.%{_arch}/extra/lustre-client/fs/osc.ko.xz" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
printf '%s\n' "/lib/modules/%{kernel}.%{_arch}/extra/lustre-client/fs/ptlrpc_gss.ko.xz" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
printf '%s\n' "/lib/modules/%{kernel}.%{_arch}/extra/lustre-client/fs/pc/ptlrpc.ko.xz" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
printf '%s\n' "/lib/modules/%{kernel}.%{_arch}/extra/lustre-client/net/libcfs.ko.xz" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
printf '%s\n' "/lib/modules/%{kernel}.%{_arch}/extra/lustre-client/net/ko2iblnd.ko.xz" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
printf '%s\n' "/lib/modules/%{kernel}.%{_arch}/extra/lustre-client/net/ksocklnd.ko.xz" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
printf '%s\n' "/lib/modules/%{kernel}.%{_arch}/extra/lustre-client/net/lnet.ko.xz" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules


%preun
mkdir -p %{_localstatedir}/lib/rpm-state/sig-kmods
rpm -ql kmod-%{pkg}-%{?epoch:%{epoch}:}%{version}-%{release}.%{_arch} | grep '/lib/modules/%{kernel}.%{_arch}/.*\.ko\.xz$' >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-remove


%postun
if [ -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules ]
then
    while read -r MODULE
    do
        if [ -f "$MODULE" ]
        then
            printf '%s\n' "$MODULE" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add
        fi
    done < %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
    rm -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
fi
if [ -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-remove ]
then
    modules=( $(cat %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-remove | sort -u -V -t '/' -k 5 -k 4,4) )
    rm -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-remove
    if [ -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add ]
    then
        printf '%s\n' "${modules[@]}" | %{_sbindir}/weak-modules --remove-modules --no-initramfs
    else
        printf '%s\n' "${modules[@]}" | %{_sbindir}/weak-modules --remove-modules
    fi
fi
if [ -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add ]
then
    modules=( $(cat %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add | sort -u -V -t '/' -k 5 -k 4,4) )
    rm -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add
    printf '%s\n' "${modules[@]}" | %{_sbindir}/weak-modules --add-modules
fi
if [ -d %{_localstatedir}/lib/rpm-state/sig-kmods ]
then
    rmdir --ignore-fail-on-non-empty %{_localstatedir}/lib/rpm-state/sig-kmods
fi


%posttrans
if [ -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules ]
then
    while read -r MODULE
    do
        if [ -f "$MODULE" ]
        then
            printf '%s\n' "$MODULE" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add
        fi
    done < %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
    rm -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
fi
if [ -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add ]
then
    modules=( $(cat %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add | sort -u -V -t '/' -k 5 -k 4,4) )
    rm -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add
    printf '%s\n' "${modules[@]}" | %{_sbindir}/weak-modules --add-modules
fi
if [ -d %{_localstatedir}/lib/rpm-state/sig-kmods ]
then
    rmdir --ignore-fail-on-non-empty %{_localstatedir}/lib/rpm-state/sig-kmods
fi


%files
%defattr(644,root,root,755)
/lib/modules/%{kernel}.%{_arch}
%license COPYING


%changelog
* Fri Sep 16 2022 Kmods SIG <sig-kmods@centosproject.org> - 1:4.18.0~348-2.15.1~1
- kABI tracking kmod package (kernel >= 4.18.0-348.el8)
